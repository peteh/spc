package com.SPC.home.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.SPC.home.dto.BookDto;
import com.SPC.home.entity.Book;
import com.SPC.home.repository.BookRepository;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;


@RestController
@RequestMapping(value = "/book")
public class BookController {

	@Autowired
    private BookRepository bookRepository;

	@ApiOperation(value = "取得書本", notes = "列出所有書本")
	@ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/getAllBook", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Book> getAll() {
        return bookRepository.findAll();
    }
	
	@ApiOperation(value = "取得書本內容", notes = "取得書本內容")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "書本資訊")})
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/getOne/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public BookDto get(@ApiParam(required = true, name = "id", value = "書本ID") 
            		   @PathVariable Integer id) {
        Book book = bookRepository.getOne(id);
        BookDto bookDto = new BookDto();
        bookDto.setId(book.getId());
        bookDto.setName(book.getName());
        bookDto.setAuthor(book.getAuthor());
        return bookDto;
    }
	
}
