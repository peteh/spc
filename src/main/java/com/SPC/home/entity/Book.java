package com.SPC.home.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class Book {
	@Id
    @GeneratedValue
    private Integer id;
	@Column(name = "name")
    private String name;
	@Column(name = "author")
    private String author;
	
}
