package com.SPC.home.service;

import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * app load時啟動
 * @author pete
 *
 */
@Slf4j
@Component
@Order(value=1)
public class ApplicationLoader implements CommandLineRunner {

	@Autowired
    private BookService bookService;
	
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

	
	@Override
	public void run(String... args) throws Exception {
//		log.info("retry測試開始");
//		try {
//            bookService.getBook();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//		log.info("retry測試結束");
//		
//		log.info("cache測試開始");
//		for(int i = 0 ; i < 30 ; i++){
//            System.out.println(sdf.format(bookService.getTime()));
//            Thread.sleep(1000);
//        }
//		log.info("cache測試結束");
		
	}

}
