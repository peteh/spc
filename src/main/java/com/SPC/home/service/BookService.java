package com.SPC.home.service;

import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

import javax.persistence.NoResultException;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

import com.SPC.home.entity.Book;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class BookService {

	private final AtomicInteger atomicInteger = new AtomicInteger(0);

	/**
	 * maxAttempts 表示最多執行三次
	 * backoff 表示間隔，當捕捉到錯誤時，停多少秒後再重試
	 * @return
	 */
    @Retryable(include = {NoResultException.class},
    			maxAttempts = 3,
    			backoff = @Backoff(value = 2000))
    public Book getBook() {
        int count = atomicInteger.incrementAndGet();
        log.info("count = " + count);
        if (count < 5) {
            throw new NoResultException();
        } else {
            return new Book();
        }
    }

    //當捕捉到 NoResultException 的時候會自動進行重試
    @Recover
    public Book recover(NoResultException e) {
        log.info("get NoResultException & return null");
        return null;
    }
    
    
    @Cacheable(cacheNames = "getTime")
    public Date getTime() {
        return new Date();
    }

    @Cacheable("currentTimeMillis")
    public Long getCurrentTimeMillis() {
        return System.currentTimeMillis();
    }
	
}
